# Dummy Setup Repository

[![pipeline status](https://gitlab.cern.ch/rpcos4ph2setups/dummy-setup/badges/master/pipeline.svg)](https://gitlab.cern.ch/rpcos4ph2setups/dummy-setup/-/commits/master)

Dummy Setup repository.

- **Configuration:** `configuration/config_file.yaml` 
- **Commands:** `commands/`
- **Monitorables:** `monitorables/` **(not implemented, yet.)**
